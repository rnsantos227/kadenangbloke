pragma solidity ^0.5.11;

contract Teacher {
    struct Instructor {
        uint id;
        string name;
        string school;
        string subject;
        uint grade;
        uint voters;
        bool isGraded;
        bool isActive;
    }

    mapping(address => uint) private points;
    mapping(uint => Instructor) public instructors;

    uint public instructorCount;
    constructor() public {
        addInstructor("Carlo Estrada Magpili", "STI Ortigas-Cainta", "ICT");
        addInstructor("Dani-Dean Sabillo", "STI Ortigas-Cainta", "ICT");
        addInstructor("Janette Villaralvo", "STI Ortigas-Cainta", "General Education (PE)");
    }

    event votedEvent (
        uint indexed _instructorID
    );

    function addInstructor (string memory _name, string memory _school, string memory _subject) public {
        ++instructorCount;
        instructors[instructorCount] = Instructor(instructorCount, _name, _school, _subject,  0, 0, false, true);
    }

    function setName(uint _instructorID, string memory _name) public {
        instructors[_instructorID].name = _name;
    }

    function setSchool(uint _instructorID, string memory _school) public {
        instructors[_instructorID].school = _school;
    }

    function setSubject(uint _instructorID, string memory _subject) public {
        instructors[_instructorID].subject = _subject;
    }

    function setPoints(uint _points) public {
        if(points[msg.sender] <= 0) {
            points[msg.sender] = points[msg.sender] + _points;
        }
    }

    function setActive(uint _instructorID, bool _status) public {
        instructors[_instructorID].isActive = _status;
    }

    function setIsGraded(uint _instructorID, bool _status) public {
        instructors[_instructorID].isGraded = _status;
    }

    function setGrade (uint _instructorID, uint grade) public {
        if(points[msg.sender] > 0 && !instructors[_instructorID].isGraded) {
            points[msg.sender] = points[msg.sender] - 1;
            instructors[_instructorID].grade = (instructors[_instructorID].grade + grade) / 2;
            instructors[_instructorID].isGraded = true;
            instructors[_instructorID].voters++;
            emit votedEvent(_instructorID);
        }
    }


}