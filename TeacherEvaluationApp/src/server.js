//modules
const Web3 = require('web3')
const app = require('express')()
const cors = require('cors')
const multer = require('multer')
const dotenv = require('dotenv')

const TeacherJSON = require('../build/contracts/Teacher.json')

app.use(cors())
app.use(multer().none())
dotenv.config();

const server = require('http').Server(app)
const io = require('socket.io')(server)
const MongoClient = require('mongodb').MongoClient;

//cloud connection
const client = new MongoClient(process.env.APP_DATABASE_URI, { 
    useNewUrlParser: true,
    useUnifiedTopology: true
});

var database

client.connect(err => {
    if (err) return console.log(err)
    database = client.db('db_evaluation')
});

web3 = new Web3(new Web3.providers.HttpProvider(process.env.APP_ETHEREUM_URI))
const contract = new web3.eth.Contract(TeacherJSON['abi'], process.env.APP_CONTRACT_ADDRESS)

//APIs
app.get('/api/', async (request, response) => {
    const accounts = await web3.eth.getAccounts()
    response.json(accounts)
})

app.get('/api/teachers',  async (request, response) => {
    const count = await contract.methods.instructorCount().call()
    let teachers = []
    for(var n = 1; n <= count; n++) {
        const teacher = await contract.methods.instructors(n).call()
        teachers.push(teacher)
    }

    response.json(teachers)
})

app.post('/api/teachers/add', async (request, response) => {
    await contract.methods.addInstructor(request.body.name, request.body.school, request.body.subject)
    .send({from: request.body.address})
    .on('receipt', function(receipt){ 
        console.log(receipt)
    })
    response.send('success')
})

app.post('/api/teachers/vote', async (request, response) => {
    await contract.methods.setGrade(request.body.teachers_id, request.body.grade)
    .send({from: request.body.address})
    .on('receipt', function(receipt){ 
        console.log(receipt)
    })
    response.send('success')
})

//socket
io.on('connection', socket => {
    console.log('User connected')
    socket.on('disconnect', () => {
      console.log('user disconnected')
    })
})
server.listen(8000, () => console.log(`Listening on port 8000`))