var Teacher = artifacts.require("./Teacher.sol")

contract("Teacher", (accounts) => {
    it("initializes the teachers", () => {
        return Teacher.deployed().then((instance) => {
            return instance.instructorCount()
        }).then(count => {
            assert.equal(count, 1)
        })
    })
})