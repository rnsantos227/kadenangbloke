import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import axios from 'axios'

import Menubar from './Menubar'

const useStyles = makeStyles({
    table: {
        minWidth: 650,
        margin: 50
    },
    rank_column: {
        width: 150
    }
})

const Index = () => {
    const classes = useStyles();
    const [data, setData] = useState({ 
        teachers: [] 
    });

    useEffect(async () => {
        const fetchData = async () => {
            const result = await axios('http://127.0.0.1:8000/api/teachers/')
            let list = result.data 
            let sortedList = list.sort((a, b) => parseFloat(a.grade) - parseFloat(b.grade))
    
            setData({
                teachers: sortedList
            })
        }
        fetchData()
    }, [])

    return(
    <div>
        <Menubar />
        <Grid container justify = "center">
            <Grid item xs={10}>
                <Table  className={classes.table} aria-label="teacher table">
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.rank_column}><b>Rank</b></TableCell>
                            <TableCell><b>ID Number</b></TableCell>
                            <TableCell><b>Teacher Name</b></TableCell>
                            <TableCell><b>Teacher School</b></TableCell>
                            <TableCell><b>Teacher Subject</b></TableCell>
                            <TableCell><b>Grade</b></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.teachers.map((teacher, index) => (
                            <TableRow key={teacher.id}>
                                <TableCell className={classes.rank_column}>{ Number(index) + 1}</TableCell>
                                <TableCell>{teacher.id.toString().padStart(7, "0")}</TableCell>
                                <TableCell>{teacher.name}</TableCell>
                                <TableCell>{teacher.school}</TableCell>
                                <TableCell>{teacher.subject}</TableCell>
                                <TableCell>{teacher.grade}</TableCell>
                            </TableRow>
                        ))}                       
                    </TableBody>
                </Table>
            </Grid>   
        </Grid> 
    </div>
    )
}

export default Index;