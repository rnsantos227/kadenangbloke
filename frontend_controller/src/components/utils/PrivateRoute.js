import React from 'react'
import Authenticate from './Authenticate'
import {
  Route,
  Redirect
} from "react-router-dom";

const PrivateRoute = ({ children, ...rest}) => {
    return (
        <Route {...rest}
            render = {({ location }) => 
            Authenticate.isAuthenticate ? (children) :
            (
                <Redirect
                    to={{
                        pathname: "/",
                        state: { from: location }
                    }}
                />
            )}
        />
    )
}

export default PrivateRoute