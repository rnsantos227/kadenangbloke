const Authenticate = {
    isAuthenticated: false,
    authenticate(cb) {
        Authenticate.isAuthenticated = true
        setTimeout(cb, 100)
    },

    signout(cb) {
        Authenticate.isAuthenticated = false
        setTimeout(cb, 100)
    }
}